#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
extern crate rocket_contrib;

mod frontend;
mod gateway;

use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;

use std::sync::Mutex;

fn main() {
    rocket::ignite()
        .mount(
            "/",
            routes![
                frontend::index,
                frontend::ipfs_file_stat,
                gateway::ipfs_gateway,
                gateway::ipns_gateway
            ],
        )
        .mount("/static", StaticFiles::from("static"))
        .attach(Template::fairing())
        .manage(frontend::ServerState {
            recent_details: Mutex::new(Vec::new()),
        })
        .launch();
}
