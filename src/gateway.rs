use reqwest::{redirect, ClientBuilder, StatusCode};
use rocket::request::FromRequest;
use rocket::response::Response;
use rocket::Outcome;
use rocket::Request;

use std::io::Cursor;
use std::path::PathBuf;

pub struct ShouldRequestWithTrailingSlash(pub bool);
impl<'a, 'r> FromRequest<'a, 'r> for ShouldRequestWithTrailingSlash {
    type Error = ();

    fn from_request(request: &'a Request) -> rocket::request::Outcome<Self, Self::Error> {
        Outcome::Success(ShouldRequestWithTrailingSlash(
            request.uri().is_normalized(),
        ))
    }
}

fn gateway_inner(
    hash: PathBuf,
    path: String,
    normalised: ShouldRequestWithTrailingSlash,
) -> Option<Response<'static>> {
    if path != "ipfs" && path != "ipns" {
        return None;
    }

    let async_runtime = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .expect("failed to build tokio runtime");

    let future = async {
        let req_client = ClientBuilder::new()
            .redirect(reqwest::redirect::Policy::custom(|attempt| {
                println!("{} {}", attempt.status(), attempt.url());
                if attempt.status() == StatusCode::FOUND {
                    attempt.stop()
                } else {
                    redirect::Policy::default().redirect(attempt)
                }
            }))
            .build()
            .unwrap();
        match req_client
            .get(format!(
                "http://localhost:8080/{}/{}{}",
                path,
                hash.display(),
                if normalised.0 { "" } else { "/" }
            ))
            .send()
            .await
        {
            Ok(resp) => {
                let headers = resp.headers().clone();
                let mut headers_resp = Response::build().finalize();
                for (key, value) in headers.iter() {
                    headers_resp = Response::build()
                        .raw_header(
                            key.as_str().to_string(),
                            value.to_str().unwrap().to_string(),
                        )
                        .join(headers_resp)
                        .finalize();
                }
                if resp.status().canonical_reason().is_some() {
                    headers_resp = Response::build()
                        .raw_status(
                            resp.status().as_u16(),
                            resp.status().canonical_reason().unwrap(),
                        )
                        .join(headers_resp)
                        .finalize();
                }
                return Some(
                    Response::build()
                        .sized_body(Cursor::new(resp.bytes().await.unwrap()))
                        .join(headers_resp)
                        .finalize(),
                );
            }
            Err(_) => None,
        }
    };

    async_runtime.block_on(future)
}

#[get("/ipfs/<hash..>")]
pub fn ipfs_gateway(
    hash: PathBuf,
    normalised: ShouldRequestWithTrailingSlash,
) -> Option<Response<'static>> {
    gateway_inner(hash, "ipfs".to_string(), normalised)
}

#[get("/ipns/<hash..>")]
pub fn ipns_gateway(
    hash: PathBuf,
    normalised: ShouldRequestWithTrailingSlash,
) -> Option<Response<'static>> {
    gateway_inner(hash, "ipns".to_string(), normalised)
}
