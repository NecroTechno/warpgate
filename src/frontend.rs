use ipfs_api::IpfsClient;
use rocket::State;
use rocket_contrib::templates::Template;
use serde::Serialize;

use std::sync::Mutex;

#[derive(Serialize)]
struct DetailsContext {
    recent_details: Vec<String>,
    hash: String,
    size: u64,
    cumulative_size: u64,
    blocks: u64,
    typ: String,
    size_local: Option<u64>,
    local: Option<bool>,
    is_website: bool,
    directory_contents: Option<Vec<DirectoryContents>>,
}

#[derive(Serialize, Clone)]
struct DirectoryContents {
    hash: String,
    name: String,
    size: u64,
    typ: u32,
}

#[get("/details?<hash>")]
pub fn ipfs_file_stat(hash: String, server_state: State<ServerState>) -> Option<Template> {
    let ipfsclient: IpfsClient = IpfsClient::default();

    let async_runtime = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .expect("failed to build tokio runtime");

    let future_stat = async {
        let stat = ipfsclient.files_stat(&format!("/ipfs/{}", hash)).await;
        match stat {
            Ok(stats) => Some(stats),
            Err(_) => None,
        }
    };

    let future_ls = async {
        match ipfsclient.ls(&format!("/ipfs/{}", hash)).await {
            Ok(result) => {
                let mut d_content_vec: Vec<DirectoryContents> = Vec::new();
                for object in result.objects {
                    for i in object.links {
                        d_content_vec.push(DirectoryContents {
                            hash: i.hash.to_owned(),
                            name: i.name.to_owned(),
                            size: i.size,
                            typ: i.typ,
                        });
                    }
                }
                Some(d_content_vec)
            }
            Err(_) => None,
        }
    };

    match async_runtime.block_on(future_stat) {
        Some(stats) => {
            let d_content = if stats.typ == "directory" {
                async_runtime.block_on(future_ls)
            } else {
                None
            };
            let mut rd = server_state.inner().recent_details.lock().unwrap();
            if !rd.iter().any(|i| i == &stats.hash) {
                rd.push(stats.hash.clone());
            } else {
                let tmp = rd.remove(0);
                rd.push(tmp);
            }
            if rd.len() > 5 {
                rd.remove(0);
            }
            let is_website = stats.typ == "directory"
                && d_content.is_some()
                && d_content
                    .clone()
                    .unwrap()
                    .iter()
                    .any(|i| i.name == "index.html");
            let context = DetailsContext {
                recent_details: rd.to_vec(),
                hash: stats.hash,
                size: stats.size,
                cumulative_size: stats.cumulative_size,
                blocks: stats.blocks,
                typ: stats.typ,
                size_local: stats.size_local,
                local: stats.local,
                is_website,
                directory_contents: d_content,
            };
            Some(Template::render("details", &context))
        }
        None => None,
    }
}

#[derive(Serialize)]
struct IndexContext {
    recent_details: Vec<String>,
}

#[get("/")]
pub fn index(server_state: State<ServerState>) -> Template {
    let context = IndexContext {
        recent_details: server_state.inner().recent_details.lock().unwrap().to_vec(),
    };
    Template::render("base", &context)
}

pub struct ServerState {
    pub recent_details: Mutex<Vec<String>>,
}
